FROM marvin21/ttrss-base:latest

LABEL maintainer="Marvin Dalheimer <me@marvin-dalheimer.de>"

ADD ./tt-rss /var/www/html
ADD ./config.php /var/www/html/config.php

ADD ./entrypoint.sh /bin/entrypoint.sh
RUN chmod +x /bin/entrypoint.sh

RUN touch /var/www/html/feed-update.sh &&\
    chmod +x /var/www/html/feed-update.sh

ADD ./feed-update.cron /var/www/html/feed-update.cron
RUN chmod 0644 /var/www/html/feed-update.cron

RUN useradd -ms /bin/bash ttrss
RUN chown -R ttrss /var/www/html

RUN gpasswd -a ttrss sudo &&\
    echo "ttrss ALL = NOPASSWD: /usr/sbin/cron" >> /etc/sudoers

USER ttrss
WORKDIR /var/www/html

EXPOSE 80
CMD ["entrypoint.sh"]
